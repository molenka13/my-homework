let firRest = document.querySelectorAll('.fir-rest')
firRest.forEach(item => {
    item.style.display = 'none'
})
document.querySelector('.fir-main-nav').addEventListener('click' , () => {
    document.querySelector('.fir-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-fir').style.display = 'flex'
    document.querySelectorAll('.service-info-rfir').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.first-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})
document.querySelector('.sec-main-nav').addEventListener('click' , () => {
    document.querySelector('.sec-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-sec').style.display = 'flex'
    document.querySelectorAll('.service-info-rsec').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.second-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})
document.querySelector('.thi-main-nav').addEventListener('click' , () => {
    document.querySelector('.thi-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-third').style.display = 'flex'
    document.querySelectorAll('.service-info-rthird').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.third-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})
document.querySelector('.four-main-nav').addEventListener('click' , () => {
    document.querySelector('.four-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-fourth').style.display = 'flex'
    document.querySelectorAll('.service-info-rfour').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.fourth-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})
document.querySelector('.five-main-nav').addEventListener('click' , () => {
    document.querySelector('.five-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-fifth').style.display = 'flex'
    document.querySelectorAll('.service-info-rfifth').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.fifth-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})
document.querySelector('.six-main-nav').addEventListener('click' , () => {
    document.querySelector('.six-main-nav').classList.add('active-main-nav')
    document.querySelector('.service-info-sixth').style.display = 'flex'
    document.querySelectorAll('.service-info-rsixth').forEach(i => i.style.display = 'none')
    document.querySelectorAll('.sixth-rest').forEach(e => {
        e.classList.remove('active-main-nav')
    })
})


let workPic = document.querySelectorAll('.work-pic')
document.querySelector('.hidden-work-pics').style.display = 'none'
document.querySelector('.btn-menu').addEventListener('click' , () => {
    document.querySelector('.hidden-work-pics').style.display = 'grid'
    document.querySelector('.btn-menu').style.display = 'none'
})
document.querySelector('.second-item').addEventListener('click' , () => {
    workPic.forEach(pic => {
        pic.style.display = 'none'
    })
    workPic.forEach(pic => {
        if(pic.classList.contains('graphic') === true){
            pic.style.display = 'grid'
        }
    })
})
document.querySelector('.third-item').addEventListener('click' , () => {
    workPic.forEach(pic => {
        pic.style.display = 'none'
    })
    workPic.forEach(pic => {
        if(pic.classList.contains('web') === true){
            pic.style.display = 'grid'
        }
    })
})
document.querySelector('.fourth-item').addEventListener('click' , () => {
    workPic.forEach(pic => {
        pic.style.display = 'none'
    })
    workPic.forEach(pic => {
        if(pic.classList.contains('landing') === true){
            pic.style.display = 'grid'
        }
    })
})
document.querySelector('.last-item').addEventListener('click' , () => {
    workPic.forEach(pic => {
        pic.style.display = 'none'
    })
    workPic.forEach(pic => {
        if(pic.classList.contains('wordpress') === true){
            pic.style.display = 'grid'
        }
    })
})
document.querySelector('.first-item').addEventListener('click' , () => {
    workPic.forEach(pic => {
        pic.style.display = 'grid'
    })
})

let firPerson = document.querySelector('.fir-person')
firPerson.addEventListener('click' , () => {
    document.querySelector('.what-people-txt').innerText = 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.'
    document.querySelector('.name').innerText = 'Angelika Ali'
    document.querySelector('.career').innerText = 'Programmer'
    document.querySelector('.person').style.display = 'none'
    document.querySelector('.people-image').insertAdjacentHTML('beforeend' , '<img class="person" src="img/woman.svg" alt="man">')
})
let secPerson = document.querySelector('.sec-person')
secPerson.addEventListener('click' , () => {
    document.querySelector('.what-people-txt').innerText = 'lorem ipsum nteger dignissim, augue tempus ultricies luctus, quam dui laoreet ultricies luctus, quam dui'
    document.querySelector('.name').innerText = 'John Ali'
    document.querySelector('.career').innerText = 'Web Designer'
    document.querySelector('.person').style.display = 'none'
    document.querySelector('.people-image').insertAdjacentHTML('beforeend' , '<img class="person" src="img/man1.svg" alt="man">')
})
let thirdPerson = document.querySelector('.other-person-three')
thirdPerson.addEventListener('click' , () => {
    document.querySelector('.what-people-txt').innerText = 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.'
    document.querySelector('.name').innerText = 'Hasan Ali'
    document.querySelector('.career').innerText = 'UX Designer'
    document.querySelector('.person').style.display = 'none'
    document.querySelector('.people-image').insertAdjacentHTML('beforeend' , '<img class="person" src="img/man.svg" alt="man">')
})
let fourthPerson = document.querySelector('.fourth-person')
fourthPerson.addEventListener('click' , () => {
    document.querySelector('.what-people-txt').innerText = 'last lorem ipsum nteger dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.'
    document.querySelector('.name').innerText = 'Liliana Ali'
    document.querySelector('.career').innerText = 'Graphic Designer'
    document.querySelector('.person').style.display = 'none'
    document.querySelector('.people-image').insertAdjacentHTML('beforeend' , '<img class="person" src="img/woman1.svg" alt="man">')
})

