const input = document.createElement('input')
const span = document.getElementById('price')
input.placeholder = 'price'
document.body.append(input)
input.addEventListener('focus' , () => {
    input.style.border = 'none'
    input.style.outline = 'none'
    input.style.border = '3px solid green'
})
input.addEventListener('blur' , () => {
    span.insertAdjacentText('afterbegin' , 'Текущая цена: ' + input)
    document.body.append(span)
})