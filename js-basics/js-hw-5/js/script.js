// экранирование - это когда мы хотим чтобы спецсимволы не имели никакого значения.Чтобы использовать специальный символ как обычный, нужно добавить к нему бэкслэш(\).Примеры спецсимволов :[ \ ^ $ . | ? * + ( )
function createNewUser() {
//    let now = new Date()
    const firName = prompt('please enter ur firstname')
    const lastName = prompt('please enter ur lastname')
    const bday = new Date(prompt('please enter your date of birth(dd.mm.yyyy)'))
    const newUser = {
        firstName: firName,
        lastname: lastName,
        birthday:bday,
        getAge() {
            return new Date(Date.now() - this.birthday).getFullYear() - 1970
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastname.toLowerCase()
        }
    }
    console.log(newUser.getAge())
    console.log(newUser.getPassword())
}
createNewUser()