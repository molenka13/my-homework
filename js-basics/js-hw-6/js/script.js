//метод forEach перебирает каждый элемент массива и выполняет заданую функцию с каждым элементом этого массива.
const filterBy = (array, type) => array.filter(item => typeof item !== type)

console.log(filterBy( ['hello', 'world', 23, '23', null],'string'))